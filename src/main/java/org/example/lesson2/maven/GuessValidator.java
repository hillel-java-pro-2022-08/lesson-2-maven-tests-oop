package org.example.lesson2.maven;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GuessValidator {
    private final int requiredNumber;

    public GuessVariant isGuess(int choose){
       if(choose == requiredNumber) return GuessVariant.GUESS;
       return choose > requiredNumber ? GuessVariant.LESS : GuessVariant.GREATER;
    }
}
