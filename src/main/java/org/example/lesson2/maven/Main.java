package org.example.lesson2.maven;

import java.util.Scanner;

public class Main {

    public static final int MIN = 1;
    public static final int MAX = 100;

    public static void main(String[] args) {
        NumberRandomizer numberRandomizer = new NumberRandomizer(MIN, MAX);
        GuessValidator validator = new GuessValidator(numberRandomizer.getNumber());
        Scanner scanner = new Scanner(System.in);
        UserCommunication communication = new UserCommunication(scanner, System.out);

        communication.showHelloMessage();
        while (true) {
            int variant = communication.readUserNumber();
            GuessVariant result = validator.isGuess(variant);
            communication.showResult(result);
            if (result == GuessVariant.GUESS) break;
        }
    }
}
