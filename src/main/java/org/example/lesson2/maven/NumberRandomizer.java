package org.example.lesson2.maven;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NumberRandomizer {
    private final int min;
    private final int max;

    int getNumber(){
        return (int) Math.round(Math.random()*(max-min)+min);
    }
}
