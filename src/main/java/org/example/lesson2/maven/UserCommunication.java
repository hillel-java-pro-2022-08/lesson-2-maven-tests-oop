package org.example.lesson2.maven;

import lombok.RequiredArgsConstructor;

import java.io.PrintStream;
import java.util.Map;
import java.util.Scanner;

@RequiredArgsConstructor
public class UserCommunication {
    private final Scanner scanner;
    private final PrintStream out;

    private final static Map<GuessVariant, String> messages = Map.of(
            GuessVariant.GUESS, "Yes, you are right!",
            GuessVariant.LESS, "Oh, no, my number is less then your, try again",
            GuessVariant.GREATER, "Oh, no, my number is greater then your, try again"
    );

    public void showHelloMessage() {
        out.println("Hello, I choose number, try to guess it!");
    }

    public int readUserNumber() {
        out.println("Enter your variant: ");
        int number = scanner.nextInt();
        scanner.nextLine();
        return number;
    }

    public void showResult(GuessVariant guessVariant) {
        String message = messages.get(guessVariant);
        out.println(message);
    }
}
