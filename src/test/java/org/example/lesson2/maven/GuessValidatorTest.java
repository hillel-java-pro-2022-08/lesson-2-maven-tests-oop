package org.example.lesson2.maven;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GuessValidatorTest {

    public static final int REQUIRED_NUMBER = 10;


    @Test
    void shouldGuessNumber() {
        //given
        GuessValidator guessValidator = new GuessValidator(REQUIRED_NUMBER);

        //when
        GuessVariant result = guessValidator.isGuess(10);
        GuessVariant resultLess = guessValidator.isGuess(15);
        GuessVariant resultGreater = guessValidator.isGuess(9);

        //then
        assertEquals(GuessVariant.GUESS, result);
        assertEquals(GuessVariant.LESS, resultLess);
        assertEquals(GuessVariant.GREATER, resultGreater);
    }

}